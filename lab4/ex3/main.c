#include <stdlib.h>

extern void assembly();

int intVar = 'A';
char* charPtr = "ala ma kota";

extern int intVarAs;
extern char charVarAs[];

int main(){

	assembly();
	
	printf("\n%d\n", intVarAs);
	printf("%s\n", charVarAs);

	return 0;
}