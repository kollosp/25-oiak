.data
    msgs: .asciz "string:"
    masks: .asciz "%s"
    outputs: .asciz "string is %s"
    msgi: .asciz "int:"
    maski: .asciz "%i"
    outputi: .asciz "int is %i"

	timeMask: .asciz "%x %x"

.bss
    .comm input, 32
    .comm inputInt, 4

.section .text
.globl main
    main:

		#call timeCounter
		#ret

		push %ebp
		mov %esp, %ebp

        pushl $msgs
        call printf
        addl $4, %esp

        pushl $input
        pushl $masks
        call scanf
        addl $8, %esp
        
        pushl $input
        pushl $outputs
        call printf
        addl $8, %esp
	
		pushl $msgi
        call printf
        addl $4, %esp

        pushl $inputInt
        pushl $maski
        call scanf
        addl $8, %esp
        
        pushl inputInt
        pushl $outputi
        call printf
        addl $8, %esp

		mov %ebp, %esp    
      	pop %ebp
 		
		mov $0, %eax
		
		ret

# %eax zrwaca wynik
timeCounter:
	push %ebp
	mov %esp, %ebp
	xor %eax, %eax
	cpuid
	rdtsc

	push %eax
	push %edx
	push $timeMask
	call printf 

	mov %ebp, %esp
	pop %ebp
	ret
	

registerTime:
	push %ebp
	mov %esp, %ebp


	
	pop %ebp 
	
 
