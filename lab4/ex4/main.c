#include <stdlib.h>
extern int timeCounter();

extern int registerTest();

extern int writeTest();
extern int memTest();
extern int printfTest();

int tests(){
	int reg=0,write=0,mem=0,prf=0;
	int i=0;

	for(;i<100;++i){
		reg += registerTest();
		write += writeTest();
		mem += memTest();
		prf += printfTest();
	}

	printf("Times: reg: %d, write: %d, mem: %d, prf: %d", reg/100, write/100, mem/100, prf/100);
}


int main()
{
	printf("%x\n", registerTest());
	printf("%x\n", writeTest());
	printf("%x\n", memTest());

	tests();	
	return 0;
}