	.file	"main.c"
	.globl	f
	.data
	.align 4
	.type	f, @object
	.size	f, 4
f:
	.long	1065353216
	.globl	k
	.bss
	.align 4
	.type	k, @object
	.size	k, 4
k:
	.zero	4
	.globl	res
	.align 4
	.type	res, @object
	.size	res, 4
res:
	.zero	4
	.section	.rodata
.LC0:
	.string	"%x\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	andl	$-16, %esp
	subl	$16, %esp
	call	fpuStat
	movl	%eax, 4(%esp)
	movl	$.LC0, (%esp)
	call	printf
	flds	f
	flds	k
	fdivrp	%st, %st(1)
	fstps	res
	call	fpuStat
	movl	%eax, 4(%esp)
	movl	$.LC0, (%esp)
	call	printf
	call	fpuStackOverflow
	call	fpuStat
	movl	%eax, 4(%esp)
	movl	$.LC0, (%esp)
	call	printf
	movl	$0, %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 4.8.4-2ubuntu1~14.04.4) 4.8.4"
	.section	.note.GNU-stack,"",@progbits
