	.file	"main.c"
	.globl	f
	.data
	.align 8
	.type	f, @object
	.size	f, 8
f:
	.long	0
	.long	1072693248
	.globl	k
	.align 8
	.type	k, @object
	.size	k, 8
k:
	.long	0
	.long	1071644672
	.globl	res
	.bss
	.align 8
	.type	res, @object
	.size	res, 8
res:
	.zero	8
	.section	.rodata
.LC0:
	.string	"%x\n"
.LC1:
	.string	"%x, %f\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	andl	$-16, %esp
	subl	$32, %esp
	call	fpuStat
	movl	%eax, 4(%esp)
	movl	$.LC0, (%esp)
	call	printf
	fldl	f
	fldl	k
	faddp	%st, %st(1)
	fstpl	res
	call	fpuSetControl
	fldl	res
	fstpl	24(%esp)
	call	fpuSetControl
	fldl	24(%esp)
	fstpl	8(%esp)
	movl	%eax, 4(%esp)
	movl	$.LC1, (%esp)
	call	printf
	fldl	f
	fldl	k
	fsubrp	%st, %st(1)
	fstpl	res
	movl	$0, %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 4.8.4-2ubuntu1~14.04.4) 4.8.4"
	.section	.note.GNU-stack,"",@progbits
