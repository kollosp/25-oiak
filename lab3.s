SYSEXIT      = 1
SYSREAD      = 3
SYSWRITE     = 4
STDOUT       = 1
STDIN		 = 0
EXIT_SUCCESS = 0

BYTES = 255
.align 32




.comm input, 10
#data: .ascii "0000000000000000000000000000000000000000000000000"
.comm data, 255 


.global _start             # wskazanie punktu wejścia do programu


clearData:

	mov $0, %ecx

	clearDataByte:
		movb $0x0, data(%ecx)
		inc %ecx
	cmp $255, %ecx
	jl clearDataByte

	ret

printData:

	mov $SYSWRITE, %eax
	mov $STDOUT, %ebx
	mov $data, %ecx
	mov $256, %edx
	int $0x80
 
	ret

# w %eax przeczytany byte ascii
# jezeli nic nie wczytano to w ebx 0
readByte: 
	mov $SYSREAD, %eax
	mov $STDIN, %ebx
	mov $input, %ecx
	mov $1, %edx
	int $0x80 	

	mov %eax, %ecx	
	cmp $0, %eax
	jg skipEaxEqZero
		mov %eax, %ebx

		ret
	skipEaxEqZero:
	
	#zapamietaj wcztany znak w eax
	mov $0, %ecx
	mov input(%ecx), %eax
	mov $1, %ebx # flaga wczytano
	#zakoncz
	ret

# funkcja dodaje na kolejna pozycje nowa wartosc
# wartosc podana w eax
appendToData:
	mov %eax, %ebx # zapamietaj warosc ktora ma byc dodana
	mov $10, %edx
	mov $255, %ecx #licznik petli
	#mov $0, %bl #bufor na przeniesienie z poprzedniego dodawania 	

	#petla wykonujaca mnozenie kolejnych bytow
	# i przenoszaca przeniesienie z poprzednich mnozen
	appendToDataLoop:

		# wyzeruj akumultor a nastepnie przenies do niego
    	# wartosc akutlanie przetwarzanego bytu bufora liczbowego
		mov $0, %eax         # wyczysc aku 
		#mov $data, %eax
		movb data(%ecx), %al # przekopiuj byte z bufora
		mul %dl              # mnozenie przez podstawe
		
		add %bl, %al         # dodaj przeniesienie z poprzedniej
		movb %al, data(%ecx) # zapisz do bufora
		
		#operacje na przeniesienu
		mov %ah, %al    #skopiuj starszy do mlodszego (operacje na mlodszym)
		and $0xFF, %eax #wygas starszy 
		mov %al, %bl    #zapamietaj przeniesienie

		dec %ecx
		cmp $0, %ecx
	
	jne	appendToDataLoop
	pop %rax
	ret

# czytaj byty az nie napotkasz na \n
# jesli \n to w eax = 1
# jezeli nie ma nic do wczytania to w eax = 0 
readLongValue:
	call clearData
	call printData

	readNextByte:

		call readByte
	
		#jezeli nic nie wczytani to w ebx bedzie zero,
		# czas zakonczyc funkcje i zwrocic komunikat ze nic nie
		# ma juz do wczytania
		cmp $0, %ebx
		jg RLVskipEbxEqZero
			
			mov $0, %eax
			ret #zakoncz funkcje wczytujaca
		RLVskipEbxEqZero:
	
		#jezeli wczytano to w eax jest wczytany znak 
		#koniec wczytywania liczby jezeli w eax jest znak
    	#konca linii '\n' = 0xA 
		cmp $0xA, %eax
		jne skipEndOfValue
			
			push %rax
			call printData 
			pop %rax
			ret
		skipEndOfValue:
		
		# przetwarzanie wprowadzonej cyfry 
		# kod ascii dla zera 48 wiec odejmujemy 48
		sub $48, %eax

		call appendToData

	jmp readNextByte 
	#end of readNextByteLoop

	ret



_start:

	#mov $15, %eax
	#call appendToData 	

	readNextValue:
	call readLongValue 
	
	#jezeli w eax jest zero to znaczy ze nie ma nic wiecej do wczytania
	cmp $0, %eax
	jne readNextValue



	#skipProgram:
	#mov $0xFF, %eax
	#mov $0xFF, %ecx	
	#mul %cl

	#mov %ah, %al
	#and $0xFF, %eax
exit:
	mov $SYSEXIT, %eax      # funkcja do wywołania - SYSEXIT
	mov $EXIT_SUCCESS, %ebx # 1 arg. -- kod wyjścia z programu
	
	int $0x80
