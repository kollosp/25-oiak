SYSEXIT      = 1
SYSREAD      = 3
SYSWRITE     = 4
STDOUT       = 1
STDIN		 = 0
EXIT_SUCCESS = 0

.align 32

.comm text, 512

.global _start             # wskazanie punktu wejścia do programu

_start:

readLoop:
	mov $SYSREAD, %eax
	mov $STDIN, %ebx
	mov $text, %ecx
	mov $1, %edx 
	int $0x80

	#return if no bytes left
	cmp $1, %eax
	jl skipReadLoop

	mov $0, %esi #counter for swapLoop
	mov $1, %al  #mask for swapLoop - 8 bits
	mov $0, %bl #buffor for swaped bits

	#swap each bit in byte
	swapLoop: 

		#cp currnet bit into dl 		
		movb text, %dl
		and %al, %dl

		#jump if dl is greater than
		cmp $0, %dl
		je skipSetZero
		mov $1, %dl
		skipSetZero:
		
		#shift buffer	
		shl $1, %bl 

		#shift mask
		shl $1, %al			

		#add dl to buffer
		add %dl, %bl

		inc %esi
		cmp $8, %esi
		jl swapLoop

	#end swapLoop

	mov %bl, text

	mov $SYSWRITE, %eax      # funkcja do wywołania - SYSWRITE
	mov $STDOUT, %ebx        # 1 arg. - syst. deskryptor stdout
	mov $text, %ecx     # 2 arg. - adres początkowy napisu
	mov $1, %edx

	int $0x80

jmp readLoop #end readLoop

skipReadLoop:
	mov $SYSEXIT, %eax      # funkcja do wywołania - SYSEXIT
	mov $EXIT_SUCCESS, %ebx # 1 arg. -- kod wyjścia z programu
	
	int $0x80
