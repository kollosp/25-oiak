SYSEXIT      = 1
SYSREAD      = 3
SYSWRITE     = 4
STDOUT       = 1
STDIN		 = 0
EXIT_SUCCESS = 0

.align 32
.comm heightString, 512
.comm height, 2

.text
	enterValue: .ascii "Wprowadz wysokosc choinki:\n"
	enterValueLen= .-enterValue
	spaceChar: .ascii " "
	starChar: .ascii "*" 
	endlChar: .ascii "\n" 

.global _start             # wskazanie punktu wejścia do programu


exit:
	mov $SYSEXIT, %eax      # funkcja do wywołania - SYSEXIT
	mov $EXIT_SUCCESS, %ebx # 1 arg. -- kod wyjścia z programu
	
	int $0x80


# funkcja wczytuje napis ze standardowego wejscia a 
# nastepnie przetwarza go na wartosc liczbowa i zapisuje
# w akumulatorze procesora (eax)
readInt: 
	mov $SYSWRITE, %eax
	mov $STDOUT, %ebx
	mov $enterValue, %ecx
	mov $enterValueLen, %edx 
	int $0x80

	mov $SYSREAD, %eax
	mov $STDIN, %ebx
	mov $heightString, %ecx
	mov $512, %edx 
	int $0x80

	# ecx licznik petli
	# edx aktualna wartosc inta (bufor)

	sub $1, %eax # usun enter jezeli wczytano z konsoli BARDZO WAZNE 
	

	mov %eax, %ecx  # ilosc wczytanych bytow przez sysread
	mov $0, %esi #licznik petli
	mov $0, %edx  #aktualny bufor

	readInt_stringToIntLoop:
		
		#przemnoz przez 10
		mov %edx, %eax # przepisz wartosc do akumulatora
		mov $10, %ebx # zapisz mnoznik w rejestrze aby mul dzialalo
		mul %ebx # przemnoz przez 10
		mov %eax, %edx # przepisz wynik mnozenia spowrotem do %edx

		# do bufora dodaj index
		movb heightString(%esi), %bl
		sub $48, %ebx 
		
		add %ebx, %edx

		inc %esi
		cmp %esi, %ecx
		jg readInt_stringToIntLoop
	
	mov %edx, %eax # zwroc wartosc przez akumulator
	ret

drawTree:
	#cl index peli zewnetrzej
	#ch index peli wewnetrznej
	#eax ilosc pozostalych wierszy
	
	mov $0, %ecx
	mov %eax, %esi #zapamietaj ilosc wierszy do wyrysownia w esi
	
	drawTree_drawRaw:
		inc %ecx

		# argument dla rysowania spacji.
		# na kazdym poziomie rysujemy o jedna spacje mniej
		# a zaczynamy od iloci rownej poziomowi
		push %rcx

		mov %esi, %eax  
		sub %ecx, %eax
		call drawSpaces

		pop %rcx
		push %rcx

		# na kazdym poziomie rysujemy o jedna wiecej gwiazdke
		mov %ecx, %eax # argument dla rysowania spacji.
		sub $1, %eax
		shl %eax
		add $1, %eax
		call drawStars
		call drawEndl

		pop %rcx

	cmp %ecx, %esi
	jg drawTree_drawRaw

	ret

#ilosc wyrysowan podana w eax
drawSpaces: 
	mov $0, %ecx

	drawSpaces_loop:
		cmp %ecx, %eax
		jle drawSpaces_loop_skip

		push %rax
		push %rcx

		mov $SYSWRITE, %eax
		mov $STDOUT, %ebx
		mov $spaceChar, %ecx
		mov $1, %edx 
		int $0x80

		pop %rcx
		pop %rax

		inc %ecx
		 
	jmp drawSpaces_loop
	drawSpaces_loop_skip:

	ret
#funkcja rysujaca eax razy 
#gwiazdke
drawStars: 
	mov $0, %ecx

	drawStars_loop:
		push %rax
		push %rcx

		mov $SYSWRITE, %eax
		mov $STDOUT, %ebx
		mov $starChar, %ecx
		mov $1, %edx 
		int $0x80

		pop %rcx
		pop %rax

		inc %ecx
		cmp %ecx, %eax 
	jg drawStars_loop
	ret

drawEndl:
	mov $SYSWRITE, %eax
	mov $STDOUT, %ebx
	mov $endlChar, %ecx
	mov $1, %edx 
	int $0x80	

	ret
	
_start:
	#wczytaj inta z konsoli (nie wliczaj entera)
	call readInt

	#narysuj drzewo o podanym rozmiarze.
	#romiar w akumulatorze
	call drawTree


	jmp exit
